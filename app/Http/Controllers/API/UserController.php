<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use App\Utilities\ProxyRequest;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Validator;

class UserController extends Controller
{

    protected $proxy;

    public function __construct(ProxyRequest $proxy)
    {
        $this->proxy = $proxy;
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'full_name' => 'required|min:3|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json([
                "message" => "The given data was invalid.",
                'errors' => $validator->errors(),
            ], 422);
        }

        $user = User::create([
            'name' => $request->get('full_name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);

        return response()->json([
            'message' => 'You have been successfully registered!',
        ], 201);
    }

    public function logout()
    {
        $userId = Auth::user()->id;
        $tokens = DB::table('oauth_access_tokens')->where('user_id', '=', $userId);

        Auth::user()->token()->revoke();
        $tokens->delete();

        return response()->json([
            'message' => 'Logged Out'
        ]);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        $credentials['email'] = $request->email;
        $credentials['password'] = $request->password;

        if (!Auth::attempt($credentials)) {
            return response()->json([
                'message' => 'Unauthorized',
            ], 401);
        }

        $resp = $this->proxy
            ->grantPasswordToken(request('email'), request('password'));

        $userId = Auth::user()->id;

        return response()->json([
            'token' => $resp->access_token,
            'expiresIn' => $resp->expires_in,
            'message' => 'You have been logged in',
            'user_id' => $userId,
        ]);
    }
}
