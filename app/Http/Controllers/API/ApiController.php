<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['auth:api']);
    }

    /**
     * Fetch jokes.
     */
    public function jokes()
    {
        try {
            $client = new Client([
                'headers' => [
                    "Content-Type" => "application/json",
                ],
            ]);

            $url = "https://official-joke-api.appspot.com/jokes/ten";

            $res = $client->get($url);

            $response = (string) $res->getBody()->getContents();
            

        } catch (\Exception $e) {
            $response = $e->getMessage();
        }

        return $response;
    }
}
