<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'API'], function () {
    Route::post('/user/login', [
        'uses' => 'UserController@login',
        'as' => '/api/user/login',
    ]);

    Route::post('/user/register', [
        'uses' => 'UserController@register',
        'as' => '/api/user/register',
    ]);

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('/user/logout', [
            'uses' => 'UserController@logout',
            'as' => '/api/user/logout',
        ]);

        Route::post('/jokes', [
            'uses' => 'ApiController@jokes',
            'as' => '/api/jokes',
        ]);
    });
});
