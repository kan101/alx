<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;

class AuthenticationTest extends TestCase
{
    public function testRegistrationRequiredFields()
    {
        $this->json('POST', 'api/user/register', ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    "full_name" => ["The full name field is required."],
                    "email" => ["The email field is required."],
                    "password" => ["The password field is required."],
                ],
            ]);

    }

    public function testPasswordRepeat()
    {
        $userData = [
            "name" => "John Doe",
            "email" => "doe@example.com",
            "password" => "demo12345",
        ];

        $this->json('POST', 'api/user/register', $userData, ['Accept' => 'application/json'])
            ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    "password" => ["The password confirmation does not match."],
                ],
            ]);
    }

    public function testRegistrationSuccessful()
    {
        $userData = [
            "full_name" => "John Doe",
            "email" => "doe@example.com",
            "password" => "demo12345",
            "password_confirmation" => "demo12345",
        ];

        $this->json('POST', 'api/user/register', $userData, ['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJsonStructure([
                "message",
            ]);
    }

    public function testLoginEmailAndPasswordRequired()
    {
        $this->json('POST', 'api/user/login')
            ->assertStatus(422)
            ->assertJson([
                "message" => "The given data was invalid.",
                "errors" => [
                    'email' => ["The email field is required."],
                    'password' => ["The password field is required."],
                ],
            ]);
    }

    public function testLoginSuccessful()
    {
        $user = factory(User::class)->create([
            'password' => bcrypt($password = 'sample123'),
        ]);

        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => $password,
        ]);

        $this->assertAuthenticatedAs($user);
    }
}
